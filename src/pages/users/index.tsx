import React from 'react';
import store from '../../store';
import { changeInputAction, addItemAction, deleteItemAction, getTodoList } from '../../reducers/actionCreators';
import './index.scss';

// components
import TodoListUI from './components/todo_list_ui';


export default class extends React.Component {

    constructor(props) {
        super(props);

        this.state = store.getState();
        this.changeInputValue = this.changeInputValue.bind(this);
        this.addItem = this.addItem.bind(this);
        this.deleteItem = this.deleteItem.bind(this);

        this.storeChange = this.storeChange.bind(this);
        store.subscribe(this.storeChange)
    }

    storeChange() {
        this.setState(store.getState());
    }

    changeInputValue(value) {
        const action = changeInputAction({ value });
        store.dispatch(action);
    }

    addItem() {
        store.dispatch(addItemAction());
    }

    deleteItem(index) {
        store.dispatch(deleteItemAction({ index }));
    }

    componentDidMount() {
        const action = getTodoList();
        store.dispatch(action);
    }

    render() {
        const { inputValue, list }: any = this.state;
        return (
            <TodoListUI
                {...{ inputValue, list }}
                changeInputValue={this.changeInputValue}
                addItem={this.addItem}
                deleteItem={this.deleteItem}
            />
        )
    }
}