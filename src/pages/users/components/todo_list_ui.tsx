import { View } from '@tarojs/components';
import { AtButton, AtInput, AtList, AtListItem } from 'taro-ui';


export default ({ inputValue, list, changeInputValue, addItem, deleteItem }) => {
    return (
        <View>
            <View>
                <AtInput
                    name='value'
                    type='text'
                    placeholder={inputValue}
                    value={inputValue}
                    onChange={changeInputValue}
                />
                <AtButton type='primary' onClick={addItem}>增加</AtButton>
            </View>
            <AtList>
                {list.map((item, k) => (
                    <AtListItem key={k} title={item.name} onClick={() => deleteItem(k)} />
                ))}
            </AtList>
        </View>
    )
}