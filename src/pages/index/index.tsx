import { Component } from 'react'
import Taro from '@tarojs/taro';
import { View, Text } from '@tarojs/components'
import { AtButton } from 'taro-ui'


import './index.scss'


export default class extends Component {
  render() {
    return (
      <View className='index'>
        <View><Text>Hello, World</Text></View>
        <AtButton type='primary' onClick={() => {
          Taro.navigateTo({ url: 'pages/users/index' })
        }}>按钮文案</AtButton>
      </View>
    )
  }
}


