import Taro from '@tarojs/taro';
import { CHANGE_INPUT, ADD_ITEM, DELETE_ITEM, GET_LIST } from './actionTypes';


export const changeInputAction = payload => ({ type: CHANGE_INPUT, payload })
export const addItemAction = () => ({ type: ADD_ITEM })
export const deleteItemAction = payload => ({ type: DELETE_ITEM, payload });
export const getListAction = payload => ({ type: GET_LIST, payload });

export const getTodoList = () => {
    return async (dispatch) => {
        try {
            const result = await Taro.request({
                url: 'http://public-api-v1.aspirantzhang.com/users',
                method: 'GET',
                data: {},
                header: { 'content-type': 'application/json' }, // 默认值
            });
            const { data } = result;
            dispatch(getListAction(data));
        } catch (error) {
            console.log({ error });
        }
    }
}