import { CHANGE_INPUT, ADD_ITEM, DELETE_ITEM, GET_LIST } from './actionTypes';


const defaultState = {
  inputValue: 'Write Something',
  list: [],
};

export default (state = defaultState, action) => {
  // console.log(state, action);

  // Reducer 里只能接受 state，不能改变 state。
  if (action.type === CHANGE_INPUT) {
    let newState = JSON.parse(JSON.stringify(state));
    newState.inputValue = action.payload.value;
    return newState;
  }

  if (action.type === ADD_ITEM) {
    let newState = JSON.parse(JSON.stringify(state));
    if (newState.inputValue) newState.list.push({ name: newState.inputValue });
    newState.inputValue = '';
    return newState;
  }

  if (action.type === DELETE_ITEM) {
    let newState = JSON.parse(JSON.stringify(state));
    newState.list.splice(action.payload.index, 1);
    return newState;
  }

  if (action.type === GET_LIST) {
    let newState = JSON.parse(JSON.stringify(state));
    newState.list = action.payload.data;
    return newState;
  }

  return state;
}