# Redux 的使用笔记

date: 2021-11-18 23:00:00

auther: weisen

文档: `https://www.redux.org.cn/`

<br/>

### 创建 store

> `src/store/index.ts`
```typescript
import { createStore } from 'redux';
import reducer from '../reducers';

const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() // Redux DevTools 配置 (chrome 扩展)
);

export default store;
```

<br/>

### 创建 reducers

> `src/reducers/index.ts`
```typescript
const defaultState = {
  inputValue: 'Write Something',
  list: [
    '早8点开晨会，分配今天的代码任务',
    '早9点和项目经理开需求沟通会',
    '晚5点组织人员进行Review代码',
  ],
};

export default (state = defaultState, action) => {
  return state;
}
```

<br/>

### 使用

> `src/pages/users/index.tsx`
```typescript
import store from '../../store';

...
constructor(props) {
    super(props);
    this.state = store.getState();
}
...
```

<br/>

### 网络

> `Taro.request(option)`
> - 示例
```typescript
// Taro.request({
//     url: 'http://public-api-v1.aspirantzhang.com/users',
//     data: {},
//     header: { 'content-type': 'application/json' }, // 默认值
//     success: function (res) {
//         console.log(res.data)
//     }
// });

const result = await Taro.request({
    url: 'http://public-api-v1.aspirantzhang.com/users',
    method: 'GET',
    data: {},
    header: { 'content-type': 'application/json' }, // 默认值
});
console.log({ result });
```

<br/>

### 中间件（redux-thunk）

> `src/store/index.ts`
```typescript
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducer from '../reducers';


// const store = createStore(
//   reducer,
//   window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
// );

const composeEnhancers = typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
    // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
  }) : compose;

const enhancer = composeEnhancers(applyMiddleware(thunk))

const store = createStore(reducer, enhancer);

export default store;
```